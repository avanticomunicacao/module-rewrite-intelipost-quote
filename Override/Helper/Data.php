<?php
namespace Avanti\RewriteIntelipostQuote\Override\Helper;

use Intelipost\Quote\Helper\Data as IntelipostHelper;

class Data extends IntelipostHelper
{
    public function checkFreeshipping(&$response)
    {
        $freeshippingMethod = $this->scopeConfig->getValue ('carriers/intelipost/freeshipping_method');
        $freeshippingText   = $this->scopeConfig->getValue ('carriers/intelipost/freeshipping_text');

        $lowerPrice = PHP_INT_MAX;
        $lowerDeliveryDate = PHP_INT_MAX;
        $lowerMethod = null;

        foreach ($response['content']['delivery_options'] as $child) {
            $deliveryMethodId = $child ['delivery_method_id'];
            $finalShippingCost = $child ['final_shipping_cost'];
            $deliveryEstimateDays = $child ['delivery_estimate_business_days'];

            switch ($freeshippingMethod) {
                case 'lower_price':
                    if ($finalShippingCost < $lowerPrice) {
                        $lowerPrice = $finalShippingCost;
                        $lowerMethod = $deliveryMethodId;
                    }
                    break;
                case 'lower_delivery_date':
                    if ($deliveryEstimateDays < $lowerDeliveryDate) {
                        $lowerDeliveryDate = $deliveryEstimateDays;
                        $lowerMethod = $deliveryMethodId;
                    }
                    break;
            }
        }
        foreach ($response ['content']['delivery_options'] as $id => $child) {
            $deliveryMethodId = $child ['delivery_method_id'];
            if (!strcmp ($deliveryMethodId, $lowerMethod)) {
                $response ['content']['delivery_options'][$id]['final_shipping_cost'] = 0;
                $response ['content']['delivery_options'][$id]['description'] = $freeshippingText;
                break;
            }
        }
    }
}