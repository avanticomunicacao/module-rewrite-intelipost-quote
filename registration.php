<?php
use Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(
    ComponentRegistrar::MODULE,
    "Avanti_RewriteIntelipostQuote",
    __DIR__
);
